from django.shortcuts import render
import requests
import json

# Create your views here.

from django.http import Http404
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponse
from .models import PoemModel
import urllib.request
from urllib.request import urlopen


def getapoem(request):
    
    response = requests.get("https://www.poemist.com/api/v1/randompoems")
    
    json_data = json.loads(response.text)
    
    title = repr(json_data[0]['title'])
    content = repr(json_data[0]['content'])
    url = repr(json_data[0]['url'])
    poet_name = repr(json_data[0]['poet']['name'])
    poet_url = repr(json_data[0]['poet']['url'])

    poem = PoemModel.objects.create(title=title, content=content, url=url, poet_name=poet_name, poet_url=poet_url)
    poem.save()
    
    return HttpResponse("The poem was saved in the database.")




    
