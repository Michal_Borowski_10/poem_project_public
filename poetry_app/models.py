from django.db import models


# Create your models here.

class PoemModel(models.Model):
    title = models.TextField(blank=True)
    content = models.TextField(blank=True)
    url = models.TextField(blank=True)
    poet_name = models.TextField(blank=True)
    poet_url = models.TextField(blank=True)

#title, content, url, poet(name, url)
